using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishTankManager : MonoBehaviour //Clase para indicar los limites de la pecera
{
    [SerializeField] Vector2 limitsX;
    [SerializeField] Vector2 limitsY;
    [SerializeField] Vector2 limitsZ;

    private static FishTankManager singleton;

    public static FishTankManager Singleton { get {  return singleton; } }

    private void Awake()
    {
        if(singleton == null)
        {
            singleton = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public Vector3 GetPointInsideFishTank()
    {

        return new Vector3(Random.Range(limitsX.x, limitsX.y), Random.Range(limitsY.x, limitsY.y), Random.Range(limitsZ.x, limitsZ.y));
      
    }
}
