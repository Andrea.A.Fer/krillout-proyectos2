using UnityEngine;
using UnityEngine.UIElements;

public class MovimientoPez : MonoBehaviour
{
    public float speed = 100.0f;
    float rotationSpeed = 30.0f;
    bool turning = false;
    bool avoiding = false; // Variable para indicar si el pez est� evitando una colisi�n
    Vector3 avoidanceDirection; // Direcci�n para evitar la colisi�n
    public float minDistance = 0.5f;

    
    [SerializeField] Vector2 limitsX;
    [SerializeField] Vector2 limitsY;
    [SerializeField] Vector2 limitsZ;
    

    [SerializeField] Vector3 destiny;
    [SerializeField] Transform originOfRayCast;
    [SerializeField] float collisionDistanceCheck = 0.2f;
    [SerializeField] LayerMask collisionLayerMask;

    // Use this for initialization
    void Start()
    {
        //speed = Random.Range(0.01f, 0.05f);
        speed = Random.Range(8.0f, 20.0f);
        UpdateDestiny();
    }

    // Called when the fish enters a trigger
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Fish") || other.gameObject.CompareTag("Object"))
        {
            avoiding = true; // Indica que el pez est� evitando una colisi�n
            avoidanceDirection = (transform.position - other.transform.position).normalized; // Calcula la direcci�n para evitar la colisi�n
        }
    }

    // Called when the fish exits a trigger
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Fish") || other.gameObject.CompareTag("Object"))
        {
            avoiding = false; // Indica que el pez ya no est� evitando una colisi�n
        }
    }

    private void UpdateDestiny ()
    {
        destiny = new Vector3(Random.Range(limitsX.x, limitsX.y), Random.Range(limitsY.x, limitsY.y), Random.Range(limitsZ.x, limitsZ.y));
        Debug.Log("update destiny" + destiny);
        this.transform.LookAt(destiny);
        //destiny = FishTankManager.Singleton.GetPointInsideFishTank();

    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(this.transform.position, destiny) < minDistance)
        {
            UpdateDestiny();
        }

        if(Physics.Raycast(originOfRayCast.position,this.transform.forward, collisionDistanceCheck, collisionLayerMask))
        {
            UpdateDestiny();
        }
        this.transform.position += this.transform.forward * (Time.deltaTime * speed);

    }
}