using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Task
{
    public string taskId;                // Identificador �nico para la tarea
    public string description;           // Descripci�n de la tarea
    public int fishIndex;                // �ndice del pez requerido
    public int requiredAmount;           // Cantidad requerida de ese pez
    public int rewardAmount;             // Cantidad de monedas como recompensa
    public bool isCompleted;             // Si la tarea est� completada
    public int fishBoughtForTask = 0;    // Contador de peces comprados para la tarea

    public int level;
    public bool rewardClaimed = false; // Aseg�rate de que esto est� en tu clase Task
    public GameObject jobPanel;          // Panel que contiene la UI de la tarea
    public TextMeshProUGUI jobText;      // Texto que describe la tarea
    public GameObject completionTick;    // Imagen de tick verde
    public Button rewardButton;          // Bot�n para reclamar la recompensa

    //QUIZ PREGUNTAS
    public bool isFishGameCompleted; // Nuevo campo para la tarea de completar el juego de im�genes de peces
    public bool isFishGameTask;

    //A�ade nuevos campos para tareas de quiz im�genes
    public bool isQuizTask;              // Indica si la tarea es un quiz
    public int requiredCorrectAnswers;   // Respuestas correctas necesarias para completar la tarea

    //A�adir campos para la tarea de decoraci�n
    public bool isDecorationTask;          // Indica si la tarea es de comprar decoraciones
    public int decorationsBoughtForTask = 0; // Contador de decoraciones compradas para la tarea
    public int requiredAmountDecoration;// Cantidad requerida de ese decoraci�n

    //Campos para la compra de comida para peces
    public bool isFishFoodTask;
    public int fishFoodBoughtForTask = 0;
    public int requiredAmountFood;

    //Campos adicionales para la tarea de limpiar la pecera
    public bool isCleaningTask;             // Indica si la tarea es de limpieza
    public int timesCleaned = 0;            // Contador de veces que se ha limpiado la pecera para esta tarea
    public int requiredCleans;              // N�mero de limpiezas requeridas para completar la tarea
}
