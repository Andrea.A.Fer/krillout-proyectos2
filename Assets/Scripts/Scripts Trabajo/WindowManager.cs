using UnityEngine;

public class WindowManager : MonoBehaviour
{
    public GameObject window;  // Arrastra tu panel de ventana aqu� desde el Inspector

    // Funci�n para controlar la visibilidad de la ventana
    public void ToggleWindow()
    {
        window.SetActive(!window.activeSelf);
    }

    // Llama a esta funci�n cuando se presione el bot�n "S�"
    public void OnYesButton()
    {
        // Aqu� puedes agregar c�digo para lo que debe hacer el bot�n "S�"
        Debug.Log("Bot�n 'S�' presionado");
    }

    // Llama a esta funci�n cuando se presione el bot�n "No"
    public void OnNoButton()
    {
        // Esto cerrar� el panel
        window.SetActive(false);
    }
}
