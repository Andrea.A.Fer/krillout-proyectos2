using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using System.Collections.Generic;

public class Tareas : MonoBehaviour
{
    public List<Task> tasks = new List<Task>();  // Lista de tareas
    public GameObject[] levelPanels;
    public int currentLevel = 1;

    public GameObject levelCompletePanel; // Aseg�rate de asignar esto en el inspector

    public MoneyManager moneyManager;

    public Tienda tienda; // Referencia al script Tienda para acceder a datos como fishPurchased, etc.

    public QuizManager quizManager; //Referencia al script QuizManager del juego de preguntas

    public TextMeshProUGUI levelText; // Texto que muestra el nivel actual




    public Image barraTarea;  // Referencia a la UI de la barra de tareas
    private float maxTareas = 100f;  // M�ximo de tareas (100% llenado de la barra)
    private float currentTarea;  // Tareas completadas actualmente

    void Start()
    {
        InitializeTasks();
        InitializeRewardButtonListeners();
        ManageTaskPanels(currentLevel); // Asegurarte de activar el primer panel correctamente
    }
    void InitializeTasks()
    {
        foreach (var task in tasks)
        {
            task.jobPanel.SetActive(true);
            task.completionTick.SetActive(false);
            task.rewardButton.gameObject.SetActive(true);
            task.rewardButton.interactable = false;
            task.jobText.text = task.description;

            if (task.isDecorationTask)
            {
                task.decorationsBoughtForTask = 0;
            }

            if (task.isFishFoodTask)
            {
                task.fishFoodBoughtForTask = 0;  // Inicializar el contador de compras de comida
            }
        }
    }
    void InitializeRewardButtonListeners()
    {
        foreach (var task in tasks)
        {
            Button rewardButtonLocal = task.rewardButton;
            rewardButtonLocal.onClick.RemoveAllListeners();
            rewardButtonLocal.onClick.AddListener(() => ClaimReward(task));
        }
    }
    void AddRewardButtonListener(Task task)
    {
        task.rewardButton.onClick.AddListener(() => ClaimReward(task));
    }
    public void CompleteTask(Task task)
    {
        if (!task.isCompleted)  // Aseg�rate de que la tarea no est� ya completada
        {
            task.isCompleted = true;
            task.completionTick.SetActive(true);
            task.rewardButton.interactable = true;
            task.jobText.fontStyle = TMPro.FontStyles.Strikethrough; // Agregar tachado al texto
            Debug.Log("Tarea completada: " + task.description);

            UpdateHealthBar();  // Actualiza la barra de salud al completar la tarea
            CheckLevelCompletion(task.level);
        }
    }
    public void CheckLevelCompletion(int level)
    {
        bool allTasksAndRewardsCompleted = true;
        foreach (var task in tasks)
        {
            if (task.level == level && (!task.isCompleted || !task.rewardClaimed))
            {
                allTasksAndRewardsCompleted = false;
                Debug.Log("Tarea o recompensa incompleta: " + task.description);
                break;
            }
        }

        if (allTasksAndRewardsCompleted)
        {
            Debug.Log("Todos las tareas y recompensas completadas, avanzando al siguiente nivel.");
            StartCoroutine(TransitionToNextLevel());
        }
    }

    private IEnumerator TransitionToNextLevel()
    {
        levelCompletePanel.SetActive(true);
        yield return new WaitForSeconds(3);

        if (currentLevel < levelPanels.Length)
        {
            levelPanels[currentLevel - 1].SetActive(false);
            currentLevel++;
            if (currentLevel - 1 < levelPanels.Length)
            {
                levelPanels[currentLevel - 1].SetActive(true);
            }
            levelText.text = "Nivel " + currentLevel;

            // Reinicia la barra de tareas al empezar un nuevo nivel
            ResetTaskBar();
            ManageTaskPanels(currentLevel);  // Aseg�rate de llamar a esto para activar el panel de tareas del nuevo nivel
            tienda.UpdateBuyButtonStates();
        }

        levelCompletePanel.SetActive(false);
    }

    void ResetTaskBar()
    {
        currentTarea = 0;
        barraTarea.fillAmount = 0; // Establece la barra a 0%
        UpdateHealthBarColor(); // Actualiza el color de la barra seg�n el nuevo valor
    }

    void ManageTaskPanels(int level)
    {
        foreach (var task in tasks)
        {
            task.jobPanel.SetActive(task.level == level);
        }

        for (int i = 0; i < levelPanels.Length; i++)
        {
            levelPanels[i].SetActive(i + 1 == level); // Activar solo el panel del nivel actual
        }
    }
    void ClaimReward(Task task)
    {
        if (task.isCompleted && !task.rewardClaimed)  // Asegura que la tarea est� completada y la recompensa no reclamada
        {
            Debug.Log($"Intentando reclamar recompensa para la tarea: {task.description}, Completada: {task.isCompleted}, Interactable: {task.rewardButton.interactable}");
            moneyManager.AddCoins(task.rewardAmount);
            task.rewardClaimed = true;
            task.rewardButton.gameObject.SetActive(false); // Desactivar bot�n de recompensa
            task.jobText.fontStyle = TMPro.FontStyles.Strikethrough; // Tachar y poner en cursiva el texto
            Debug.Log("Recompensa reclamada para la tarea: " + task.description);

            UpdateHealthBar();  // Actualiza la barra de salud al reclamar la recompensa
            CheckLevelCompletion(task.level);
        }
    }
    public void ActivateJob(string taskId)
    {
        Task taskToActivate = tasks.Find(task => task.taskId == taskId);
        if (taskToActivate != null)
        {
            taskToActivate.jobPanel.SetActive(true);
            taskToActivate.jobText.text = taskToActivate.description;
            taskToActivate.fishBoughtForTask = 0; // Resetear el contador de peces comprados para la tarea
            taskToActivate.rewardButton.interactable = false; // Asegurarse de que el bot�n no sea interactuable hasta que la tarea est� completa
        }
        else
        {
            Debug.Log("Tarea con ID " + taskId + " no encontrada.");
        }
    }

    public void CheckFishPurchase(int fishType)
    {
        foreach (var task in tasks)
        {
            if (!task.isCompleted && fishType == task.fishIndex)
            {
                task.fishBoughtForTask++;
                if (task.fishBoughtForTask >= task.requiredAmount)
                {
                    CompleteTask(task);
                }
            }
        }
    }

    //Tarea del Quiz de preguntas
    public void UpdateQuizTaskProgress(int correctAnswers)
    {
        foreach (var task in tasks)
        {
            if (task.isQuizTask && !task.isCompleted && correctAnswers >= task.requiredCorrectAnswers)
            {
                Debug.Log("Completando tarea de quiz: " + task.description);
                CompleteTask(task);
            }
        }
    }

    void UpdateHealthBar()
    {
        int completedTasks = 0;
        foreach (var task in tasks)
        {
            if (task.isCompleted && task.level == currentLevel)
                completedTasks++;
        }

        currentTarea = (completedTasks / (float)CountTasksInCurrentLevel()) * maxTareas;
        barraTarea.fillAmount = currentTarea / maxTareas;
        UpdateHealthBarColor();
    }

    void UpdateHealthBarColor()
    {
        if (currentTarea < maxTareas * 0.25f)
            barraTarea.color = Color.red; // La barra es roja si la tarea est� por debajo del 25%
        else if (currentTarea < maxTareas * 0.5f)
            barraTarea.color = Color.yellow; // La barra es amarilla si la tarea est� entre 25% y 50%
        else
            barraTarea.color = Color.green; // La barra es verde si la tarea supera el 50%
    }

    int CountTasksInCurrentLevel()
    {
        int count = 0;
        foreach (var task in tasks)
        {
            if (task.level == currentLevel) count++;
        }
        return count;
    }

    ///M�todo para verificar y actualizar las tareas de compra de decoraci�n

    public void CheckDecorationPurchase(int decorationIndex)
    {
        foreach (var task in tasks)
        {
            if (task.isDecorationTask && !task.isCompleted)
            {
                task.decorationsBoughtForTask++;
                if (task.decorationsBoughtForTask >= task.requiredAmountDecoration)
                {
                    CompleteTask(task);
                }
            }
        }
    }
    public void CheckFishFoodPurchase()
    {
        foreach (var task in tasks)
        {
            if (task.isFishFoodTask && !task.isCompleted)
            {
                task.fishFoodBoughtForTask++;
                if (task.fishFoodBoughtForTask >= task.requiredAmountFood)
                {
                    CompleteTask(task);
                }
            }
        }
    }

    public void CleanTank()
    {
        foreach (Task task in tasks)
        {
            if (task.isCleaningTask && !task.isCompleted)
            {
                task.timesCleaned++;
                if (task.timesCleaned >= task.requiredCleans)
                {
                    CompleteTask(task);
                }
            }
        }
    }
}

