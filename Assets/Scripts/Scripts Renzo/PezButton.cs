

using UnityEngine;
using UnityEngine.UI;

public class PezButton : MonoBehaviour
{
    public Educativo pez; // El pez asociado a este bot�n
    public ButtonController buttonController;

   

    public void ButtonClicked()
    {
        // Cuando se hace clic en el bot�n, llama a la funci�n MostrarValores en el ButtonController
        if (buttonController != null && pez != null)
        {
            buttonController.MostrarValores(pez);
        }
    }
}
