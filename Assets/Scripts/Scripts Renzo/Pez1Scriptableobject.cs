using UnityEngine;

[CreateAssetMenu(fileName = "NewFishButtonData", menuName = "ButtonData/FishButtonData")]
public class FishButtonData : ScriptableObject
{
    public Sprite fishImage;
    public Sprite habitatImage;
    public string buttonText;
}
