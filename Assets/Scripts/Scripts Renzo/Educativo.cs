using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  
[CreateAssetMenu(fileName="New Fish",menuName="Fish")]
public class Educativo : ScriptableObject
{
    public string Name;
    public string Descripcion;
     public Sprite Habitat;
    public Sprite Pez;
    public string PH;
    
    public string Temperature;
    
}
