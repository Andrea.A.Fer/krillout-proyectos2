using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LogicaPantallaCompleta : MonoBehaviour
{
    public Toggle toggle;
    public TMP_Dropdown resolucionesDropDown; // Cambi� el nombre de la variable para que coincida con la declaraci�n
    Resolution[] resoluciones;

    // Start is called before the first frame update
    void Start()
    {
        // Comprobamos si la pantalla est� en modo completo al inicio y configuramos el Toggle en consecuencia
        if (Screen.fullScreen)
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }

        RevisarResolucion();
    }

    // Esta funci�n ser� llamada cuando el jugador cambie el estado del Toggle
    public void ActivarPantallaCompleta(bool pantallaCompleta)
    {
        // Configuramos el modo de pantalla completa basado en el estado del Toggle
        Screen.fullScreen = pantallaCompleta;
    }

    public void RevisarResolucion()
    {
        resoluciones = Screen.resolutions;
        resolucionesDropDown.ClearOptions();
        List<string> opciones = new List<string>();
        int resolucionActual = 0;

        for (int i = 0; i < resoluciones.Length; i++)
        {
            string opcion = resoluciones[i].width + "x" + resoluciones[i].height;
            opciones.Add(opcion);

            if (Screen.fullScreen && resoluciones[i].width == Screen.currentResolution.width && resoluciones[i].height == Screen.currentResolution.height)
            {
                resolucionActual = i;
            }
        }
        resolucionesDropDown.AddOptions(opciones); // Cambi� Opciones por opciones para que coincida con el nombre de la variable
        resolucionesDropDown.value = resolucionActual;
        resolucionesDropDown.RefreshShownValue(); // Cambi� RefeshShownValue por RefreshShownValue

        // Tambi�n hay un problema aqu�, resolucionesDropDown.value ya fue configurado arriba, no necesitas configurarlo nuevamente aqu�
        // Elimina la siguiente l�nea
        // resolucionesDropDown.value = PlayerPrefs.GetInt("numeroResolucion", 0);
    }

    public void CambiarResolucion(int indiceResolucion)
    {
        PlayerPrefs.SetInt("numeroResolucion", resolucionesDropDown.value);
        Resolution resolucion = resoluciones[indiceResolucion];
        Screen.SetResolution(resolucion.width, resolucion.height, Screen.fullScreen);
    }
}
