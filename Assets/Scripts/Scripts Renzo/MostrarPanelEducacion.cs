using UnityEngine;
using UnityEngine.UI;

public class LogicaFullScreen : MonoBehaviour
{
    public Toggle toggle;

    // Start is called before the first frame update
    void Start()
    {
        // Comprobamos si la pantalla est� en modo completo al inicio y configuramos el Toggle en consecuencia
        if (Screen.fullScreen)
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // No hay necesidad de hacer nada en este m�todo en este caso, puedes dejarlo vac�o o eliminarlo
    }

    // Esta funci�n ser� llamada cuando el jugador cambie el estado del Toggle
    public void ActivarPantallaCompleta(bool pantallaCompleta)
    {
        // Configuramos el modo de pantalla completa basado en el estado del Toggle
        Screen.fullScreen = pantallaCompleta;
    }
}
