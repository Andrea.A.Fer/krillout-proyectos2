using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; // Necesario para usar EventSystem

public class CameraMovement : MonoBehaviour
{
    public GameObject target;
    public Vector3 defaultPosition; // Posici�n original de la c�mara
    public Quaternion defaultRotation; // Rotaci�n original de la c�mara
    float speed = 5;

    float minFov = 35f;
    float maxFov = 100f;
    float sensitivity = 17f;

    public bool isMenuOpen = false;

    private float currentRotationY = 0f; // Rastrea la rotaci�n actual de la c�mara alrededor del eje Y

    void Start()
    {
        // Guarda la posici�n y rotaci�n originales de la c�mara
        defaultPosition = transform.position;
        defaultRotation = transform.rotation;
        currentRotationY = transform.eulerAngles.y;
    }

    void Update()
    {
        // Verifica si el cursor est� sobre un objeto de la UI
        if (EventSystem.current.IsPointerOverGameObject())
        {
            // Si es as�, no permite el movimiento de la c�mara
            return;
        }

        if (Input.GetMouseButton(1))
        {
            float rotationInput = Input.GetAxis("Mouse X") * speed;
            float targetRotationY = currentRotationY + rotationInput;

            // Calcula la rotaci�n deseada respecto a la rotaci�n original
            float rotationDifference = Mathf.DeltaAngle(defaultRotation.eulerAngles.y, targetRotationY);

            // Ajusta los l�mites aqu� para permitir 90 grados de rotaci�n a cada lado
            if (rotationDifference > 90f) // Cambiado de 60 a 90 grados
            {
                rotationInput = 0f;
                targetRotationY = defaultRotation.eulerAngles.y + 90f;
            }
            else if (rotationDifference < -90f) // Cambiado de 60 a 90 grados
            {
                rotationInput = 0f;
                targetRotationY = defaultRotation.eulerAngles.y - 90f;
            }

            transform.RotateAround(target.transform.position, Vector3.up, rotationInput);
            currentRotationY = targetRotationY;
        }

        // Zoom
        float fov = Camera.main.fieldOfView;
        fov += Input.GetAxis("Mouse ScrollWheel") * -sensitivity;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        Camera.main.fieldOfView = fov;

        // Resetear la c�mara con la tecla 'r'
        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetCamera();
        }
    }

    // M�todo para resetear la c�mara a la posici�n y rotaci�n originales
    void ResetCamera()
    {
        transform.position = defaultPosition;
        transform.rotation = defaultRotation;
        currentRotationY = transform.eulerAngles.y;
    }
}