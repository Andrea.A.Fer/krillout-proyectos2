using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Room : MonoBehaviour
{
    public GameObject target;
    public Vector3 defaultPosition; // Posici�n original de la c�mara
    public Quaternion defaultRotation; // Rotaci�n original de la c�mara
    float speed = 5;

    float minFov = 35f;
    float maxFov = 100f;
    float sensitivity = 17f;

    public bool isMenuOpen = false;

    void Start()
    {
        // Guarda la posici�n y rotaci�n originales de la c�mara
        defaultPosition = transform.position;
        defaultRotation = transform.rotation;
    }

    void Update()
    {
        // Verifica si el cursor est� sobre un objeto de la UI
        if (EventSystem.current.IsPointerOverGameObject())
        {
            // Si es as�, no permite el movimiento de la c�mara
            return;
        }

        if (Input.GetMouseButton(1))
        {
            transform.RotateAround(target.transform.position, transform.up, Input.GetAxis("Mouse X") * speed);
            //transform.RotateAround(target.transform.position, transform.right, Input.GetAxis("Mouse Y") * -speed);
        }

        // Resetear la c�mara con la tecla 'r'
        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetCamera();
        }
    }

    // M�todo para resetear la c�mara a la posici�n y rotaci�n originales
    void ResetCamera()
    {
        transform.position = defaultPosition;
        transform.rotation = defaultRotation;
    }
}
