﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class QuizManagerPeces : MonoBehaviour
{
    public static QuizManagerPeces instance; //Instance to make is available in other scripts without reference

    //Scriptable data which store our questions data
    [SerializeField] 
    private QuizDataScriptable questionDataScriptable;
    [SerializeField] 
    private Image questionImage;           //image element to show the image
    [SerializeField] 
    private WordData[] answerWordList;     //list of answers word in the game
    [SerializeField] 
    private WordData[] optionsWordList;    //list of options word in the game


    private GameStatus gameStatus = GameStatus.Playing;     //to keep track of game status
    private char[] wordsArray = new char[12];               //array which store char of each options

    private List<int> selectedWordsIndex;                   //list which keep track of option word index w.r.t answer word index
    private int currentAnswerIndex = 0, currentQuestionIndex = 0;   //index to keep track of current answer and current question
    private bool correctAnswer = true;                      //bool to decide if answer is correct or not
    private string answerWord;
    //string to store answer of current question

    [SerializeField]
    public GameObject gameCompletePanel;
    public GameObject gamePanel;

    public Tareas tareas;

    public GameObject quizPanel;  // Panel que contiene el quiz
    public GameObject warningPanel;  // Panel para mostrar la advertencia, asegúrate de asignar esto en el Inspector
    public Button quizButton;  // Botón para abrir el quiz
    public GameObject[] interactiveElements; // Elementos interactivos del quiz como botones de opción, etc.

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        selectedWordsIndex = new List<int>();           //create a new list at start
        SetQuestion();

        // Inicializa el quiz cerrado y la advertencia no visible
        quizPanel.SetActive(false);
        warningPanel.SetActive(false);

        // Añade un listener al botón del quiz
        quizButton.onClick.AddListener(TryOpenQuiz);
        ToggleInteractivity(false); // Desactiva la interactividad del quiz al inicio//set question
    }

    void TryOpenQuiz()
    {
        if (tareas.currentLevel == 1)
        {
            // Si el jugador está en el nivel 1, muestra el panel de advertencia
            warningPanel.SetActive(true);
            ToggleInteractivity(false); // Desactiva la interactividad del quiz
        }
        else if (tareas.currentLevel == 3)
        {
            // Si el jugador está en el nivel 2, abre el quiz y activa la interactividad
            quizPanel.SetActive(true);
            warningPanel.SetActive(false);
            ToggleInteractivity(true); // Activa la interactividad del quiz
        }
    }
    void ToggleInteractivity(bool isActive)
    {
        // Activa o desactiva todos los elementos interactivos del quiz
        foreach (var element in interactiveElements)
        {
            var button = element.GetComponent<Button>();
            if (button != null)
                button.interactable = isActive;
        }
    }

    public void CloseWarningPanel()
    {
        // Método para cerrar el panel de advertencia
        warningPanel.SetActive(false);
        // Considera qué hacer con la interactividad del quiz aquí, si es necesario
    }
    void SetQuestion()
    {
        gameStatus = GameStatus.Playing;                //set GameStatus to playing 

        //set the answerWord string variable
        answerWord = questionDataScriptable.questions[currentQuestionIndex].answer;
        //set the image of question
        questionImage.sprite = questionDataScriptable.questions[currentQuestionIndex].questionImage;
            
        ResetQuestion();                               //reset the answers and options value to orignal     

        selectedWordsIndex.Clear();                     //clear the list for new question
        Array.Clear(wordsArray, 0, wordsArray.Length);  //clear the array

        //add the correct char to the wordsArray
        for (int i = 0; i < answerWord.Length; i++)
        {
            wordsArray[i] = char.ToUpper(answerWord[i]);
        }

        //add the dummy char to wordsArray
        for (int j = answerWord.Length; j < wordsArray.Length; j++)
        {
            wordsArray[j] = (char)UnityEngine.Random.Range(65, 90);
        }

        wordsArray = ShuffleList.ShuffleListItems<char>(wordsArray.ToList()).ToArray(); //Randomly Shuffle the words array

        //set the options words Text value
        for (int k = 0; k < optionsWordList.Length; k++)
        {
            optionsWordList[k].SetWord(wordsArray[k]);
        }

    }

    public void GameComplete()
    {
        gameCompletePanel.SetActive(true);
        StartCoroutine(DeactivateGamePanel());

        // Marca la tarea específica del juego de peces como completada.
        MarkFishGameTaskAsCompleted();
    }

    private void MarkFishGameTaskAsCompleted()
    {
        // Asume que 'isFishGameTask' es un booleano que identifica las tareas del juego de peces
        foreach (Task task in tareas.tasks)
        {
            if (task.isFishGameTask && !task.isCompleted)
            {
                tareas.CompleteTask(task);  // Llama a CompleteTask en Tareas
            }
        }
    }

    private IEnumerator DeactivateGamePanel()
    {
        // Espera unos segundos antes de desactivar el panel del juego
        yield return new WaitForSeconds(3f); // Cambia el tiempo según lo que necesites

        // Desactiva el panel del juego
        gamePanel.SetActive(false);
    }

   

    //Method called on Reset Button click and on new question
    public void ResetQuestion()
    {
        //activate all the answerWordList gameobject and set their word to "_"
        for (int i = 0; i < answerWordList.Length; i++)
        {
            answerWordList[i].gameObject.SetActive(true);
            answerWordList[i].SetWord('_');
        }

        //Now deactivate the unwanted answerWordList gameobject (object more than answer string length)
        for (int i = answerWord.Length; i < answerWordList.Length; i++)
        {
            answerWordList[i].gameObject.SetActive(false);
        }

        //activate all the optionsWordList objects
        for (int i = 0; i < optionsWordList.Length; i++)
        {
            optionsWordList[i].gameObject.SetActive(true);
        }

        currentAnswerIndex = 0;
    }

    /// <summary>
    /// When we click on any options button this method is called
    /// </summary>
    /// <param name="value"></param>
    public void SelectedOption(WordData value)
    {
        // Si el estado del juego es "Siguiente" o el índice de la respuesta actual es mayor o igual a la longitud de la palabra respuesta, regresa.
        if (gameStatus == GameStatus.Next || currentAnswerIndex >= answerWord.Length) return;

        selectedWordsIndex.Add(value.transform.GetSiblingIndex()); // Añade el índice del elemento seleccionado a la lista de índices seleccionados.
        value.gameObject.SetActive(false); // Desactiva el objeto de opción seleccionado.
        answerWordList[currentAnswerIndex].SetWord(value.wordValue); // Actualiza la lista de palabras respuesta.

        currentAnswerIndex++;   // Incrementa el índice de la respuesta actual.

        // Si el índice de la respuesta actual es igual a la longitud de la palabra respuesta.
        if (currentAnswerIndex == answerWord.Length)
        {
            correctAnswer = true;   // Valor predeterminado.
                                    // Revisa cada elemento de la lista de palabras respuesta.
            for (int i = 0; i < answerWord.Length; i++)
            {
                // Si el carácter de la palabra respuesta no coincide con el valor de la palabra en la lista de palabras respuesta.
                if (char.ToUpper(answerWord[i]) != char.ToUpper(answerWordList[i].wordValue))
                {
                    correctAnswer = false; // Establece como falso.
                    break; // Sale del bucle.
                }
            }

            // Si la respuesta es correcta.
            if (correctAnswer)
            {
                Debug.Log("Respuesta Correcta");
                gameStatus = GameStatus.Next; // Cambia el estado del juego a "Siguiente".
                currentQuestionIndex++; // Incrementa el índice de la pregunta actual.

                // Si el índice de la pregunta actual es menor que el número total de preguntas disponibles.
                if (currentQuestionIndex < questionDataScriptable.questions.Count)
                {
                    Invoke("SetQuestion", 0.5f); // Pasa a la siguiente pregunta.
                }
                else
                {
                    Debug.Log("Juego Completado"); // De lo contrario, el juego se ha completado.
                    GameComplete(); // Llama al método que maneja la finalización del juego.
                }
            }
        }
    }

    public void ResetLastWord()
    {
        if (selectedWordsIndex.Count > 0)
        {
            int index = selectedWordsIndex[selectedWordsIndex.Count - 1];
            optionsWordList[index].gameObject.SetActive(true);
            selectedWordsIndex.RemoveAt(selectedWordsIndex.Count - 1);

            currentAnswerIndex--;
            answerWordList[currentAnswerIndex].SetWord('_');
        }
    }

}

[System.Serializable]
public class QuestionData
{
    public Sprite questionImage;
    public string answer;
}

public enum GameStatus
{
   Next,
   Playing
}
