using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class QuizManager : MonoBehaviour
{
    public List<QuestionAndAnswers> QnA;
    private List<QuestionAndAnswers> originalQnA; // Variable para almacenar una copia de respaldo de las preguntas originales

    public GameObject[] options;
    public int currentQuestion;
    public GameObject panelToClose; // Asigna esto en el Inspector
    public Button playButton;
    public TextMeshProUGUI QuestionTxt;

    public int countCorrectAnswers = 0;
    public int countWrongAnswers = 0;

    public TextMeshProUGUI correctAnswersText;
    public TextMeshProUGUI wrongAnswersText;

    public int requiredCorrectAnswers = 20;  // Establece un umbral de respuestas correctas
    public GameObject retryPanel;  // Panel para mostrar la opci�n de reintentar

    public Tareas tareas;

    public GameObject quizPanel;  // Panel que contiene el quiz
    public GameObject warningPanel;  // Panel para mostrar la advertencia, aseg�rate de asignar esto en el Inspector
    public Button quizButton;  // Bot�n para abrir el quiz
    public GameObject[] interactiveElements; // Elementos interactivos del quiz como botones de opci�n, etc.

    private void Start()
    {
        originalQnA = new List<QuestionAndAnswers>(QnA);
        generateQuestion();
        quizPanel.SetActive(false);
        warningPanel.SetActive(false);
        retryPanel.SetActive(false); // Inicialmente, el retryPanel debe estar oculto
        quizButton.onClick.AddListener(TryOpenQuiz);
        ToggleInteractivity(false);
    }

    void TryOpenQuiz()
    {
        if (tareas.currentLevel == 1)
        {
            // Si el jugador est� en el nivel 1, muestra el panel de advertencia
            warningPanel.SetActive(true);
            ToggleInteractivity(false); // Desactiva la interactividad del quiz
        }
        else if (tareas.currentLevel == 2)
        {
            // Si el jugador est� en el nivel 2, abre el quiz y activa la interactividad
            quizPanel.SetActive(true);
            warningPanel.SetActive(false);
            ToggleInteractivity(true); // Activa la interactividad del quiz
        }
    }

    void ToggleInteractivity(bool isActive)
    {
        // Activa o desactiva todos los elementos interactivos del quiz
        foreach (var element in interactiveElements)
        {
            var button = element.GetComponent<Button>();
            if (button != null)
                button.interactable = isActive;
        }
    }

    public void CloseWarningPanel()
    {
        // M�todo para cerrar el panel de advertencia
        warningPanel.SetActive(false);
        // Considera qu� hacer con la interactividad del quiz aqu�, si es necesario
    }


    public void correct()
    {
        countCorrectAnswers++;
        correctAnswersText.text = "Preguntas correctas: " + countCorrectAnswers;

        // Llamar a UpdateQuizTaskProgress en Tareas
        tareas.UpdateQuizTaskProgress(countCorrectAnswers);

        Debug.Log("Respuesta correcta, llamando a UpdateQuizTaskProgress con " + countCorrectAnswers + " respuestas correctas.");

        QnA.RemoveAt(currentQuestion);
        generateQuestion();
    }

    public void wrong()
    {
        countWrongAnswers++;
        wrongAnswersText.text = "Preguntas incorrectas: " + countWrongAnswers;

        QnA.RemoveAt(currentQuestion);
        generateQuestion();
    }

    void SetAnswers()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<AnswerScript>().isCorrect = false;
            options[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = QnA[currentQuestion].Answers[i];

            if (QnA[currentQuestion].CorrectAnswer == i + 1)
            {
                options[i].GetComponent<AnswerScript>().isCorrect = true;
            }
        }
    }

    void generateQuestion()
    {
        if (QnA.Count > 0)
        {
            currentQuestion = Random.Range(0, QnA.Count);
            QuestionTxt.text = QnA[currentQuestion].Question;
            SetAnswers();
        }
        else
        {
            Debug.Log("Out of Questions");

            // Comprueba si el jugador ha alcanzado el m�nimo requerido de respuestas correctas
            if (countCorrectAnswers < requiredCorrectAnswers)
            {
                retryPanel.SetActive(true);  // Mostrar el panel de reintentar
                ToggleInteractivity(false);
            }
            else
            {
                // Restaura la lista de preguntas original
                QnA = new List<QuestionAndAnswers>(originalQnA);
                Invoke("ClosePanel", 1);  // Cierra el panel despu�s de 1 segundo
            }
        }
    }

    void ShowRetryOption()
    {
        // Activa el retryPanel para preguntar al jugador si desea reintentar
        retryPanel.SetActive(true);
    }

    public void RetryQuiz()
    {
        countCorrectAnswers = 0;
        countWrongAnswers = 0;
        correctAnswersText.text = "Preguntas correctas: 0";
        wrongAnswersText.text = "Preguntas incorrectas: 0";
        QnA = new List<QuestionAndAnswers>(originalQnA);
        generateQuestion();
        quizPanel.SetActive(true);
        retryPanel.SetActive(false); // Oculta el retryPanel despu�s de elegir reintentar
        ToggleInteractivity(true);  // Reactiva la interactividad del quiz
    }

    public void ClosePanel()
    {
        if (panelToClose != null)
        {
            panelToClose.SetActive(false); // Desactiva el panel
        }
    }

    //// M�todo para desactivar el bot�n temporalmente
    //void DisableButtonTemporarily()
    //{
    //    playButton.interactable = false; // Desactiva el bot�n
    //    Invoke("EnableButton", 20); // Programa para reactivar el bot�n despu�s de 20 segundos
    //}

    //void EnableButton()
    //{
    //    playButton.interactable = true; // Reactiva el bot�n
    //}
}
