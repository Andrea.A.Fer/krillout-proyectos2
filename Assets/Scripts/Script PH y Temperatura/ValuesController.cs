using UnityEngine;
using TMPro;
using System.Collections;

public class ValuesController : MonoBehaviour
{
    public delegate void ValueChanged();
    public event ValueChanged OnPHToDefault;
    public event ValueChanged OnTemperatureToDefault;

    public event ValueChanged OnPHChange;
    public event ValueChanged OnTemperatureChange;

    public TMP_Text pHText;
    public TMP_Text temperatureText;
    private int pH = 7; // Valor inicial de pH
    private float temperature = 25.0f; // Valor inicial de temperatura

    private int defaultPH = 7; // Valor inicial de pH
    private float defaultTemperature = 25.0f; // Valor inicial de temperatura

    public TMP_Text advertencia; 

    void Start()
    {
        // Configurar el texto de advertencia inicialmente vac�o o seg�n sea necesario
        if (pH == defaultPH && Mathf.Approximately(temperature, defaultTemperature))
        {
            advertencia.text = "";  // No mostrar advertencia si todo est� en valores normales
        }
        else
        {
            advertencia.text = "Advertencia: �Valores fuera de lo normal!";
            advertencia.color = Color.red;
        }

        // Actualiza los textos de pH y temperatura
        UpdateTexts();

        // Inicia las rutinas de cambio despu�s de un retraso inicial
        StartCoroutine(StartAfterDelay());
    }

    IEnumerator StartAfterDelay()
    {
        yield return new WaitForSeconds(145);
        StartCoroutine(ChangePH());
        StartCoroutine(ChangeTemperature());
    }

    void UpdateTexts()
    {
        pHText.text = pH.ToString("F1");
        pHText.color = pH == defaultPH ? Color.green : Color.red;

        temperatureText.text = temperature.ToString("F1") + "�C";
        temperatureText.color = Mathf.Approximately(temperature, defaultTemperature) ? Color.green : Color.red;

        // Actualizar texto de advertencia si los valores no son los iniciales
        if (pH != defaultPH || !Mathf.Approximately(temperature, defaultTemperature))
        {
            advertencia.text = "Advertencia: �Valores fuera de lo normal!";
            advertencia.color = Color.red;
        }
        else
        {
            advertencia.text = ""; // No mostrar advertencia si todo est� en valores normales
        }
    }

    IEnumerator ChangePH()
    {
        while (true)
        {
            int oldPH = pH;
            pH += 1; // Cambio de pH para el ejemplo
            pH = Mathf.Clamp(pH, 4, 10); // Aseguramos que pH est� entre 0 y 14
            if (pH != oldPH)
            {
                OnPHChange?.Invoke();
            }
            UpdateTexts();
            yield return new WaitForSeconds(65);
        }
    }

    IEnumerator ChangeTemperature()
    {
        while (true)
        {
            float oldTemperature = temperature;
            temperature += 1.0f; // Cambio de temperatura para el ejemplo
            temperature = Mathf.Clamp(temperature, 22, 28); // Aseguramos que la temperatura est� entre 0�C y 28�C
            if (!Mathf.Approximately(temperature, oldTemperature))
            {
                OnTemperatureChange?.Invoke();
            }
            UpdateTexts();
            yield return new WaitForSeconds(80);
        }
    }

    public void IncrementPH()
    {
        pH += 1;
        CheckPHChange();
        UpdateTexts(); // Actualiza la interfaz de usuario si es necesario
    }

    public void DecrementPH()
    {
        pH -= 1;
        CheckPHChange();
    }

    private void CheckPHChange()
    {
        pH = Mathf.Clamp(pH, 4, 10); // Restricci�n de l�mites para pH
        if (pH == defaultPH)
        {
            OnPHToDefault?.Invoke();
        }
        UpdateTexts();
    }

    public void IncrementTemperature()
    {
        temperature += 1.0f;
        CheckTemperatureChange();
        UpdateTexts(); // Actualiza la interfaz de usuario si es necesario
    }

    public void DecrementTemperature()
    {
        temperature -= 1.0f;
        CheckTemperatureChange();
    }

    private void CheckTemperatureChange()
    {
        temperature = Mathf.Clamp(temperature, 22, 28); // Restricci�n de l�mites para temperatura
        if (Mathf.Approximately(temperature, defaultTemperature))
        {
            OnTemperatureToDefault?.Invoke();
        }
        UpdateTexts();
    }

    public void ResetValues()
    {
        pH = defaultPH;
        temperature = defaultTemperature;
        OnPHToDefault?.Invoke();
        OnTemperatureToDefault?.Invoke();
        UpdateTexts();
    }

}
