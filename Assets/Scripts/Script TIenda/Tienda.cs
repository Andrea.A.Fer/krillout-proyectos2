using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using System.Collections.Generic;

public class Tienda : MonoBehaviour
{
    private int[] saltwaterFishPrices = { 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68 };
    public Button[] buySaltwaterFishButtons;
    public Button feedFishButton;
    public GameObject fishFoodObject;

    public int[] decorationPrices = { 20, 20, 20, 25, 25, 30, 30, 35, 25, 30, 30 };
    public Button[] buyDecorationButtons;

    public MoneyManager moneyManager;
    public Tareas tareas;
    public ControladorPecesTienda controladorPecesTienda;

    public DirtParticleManager dirtParticleManager;


    [SerializeField]
    private int maxFishCount = 15; // M�ximo n�mero de peces permitidos
    public int currentFishCount = 0; // Contador de peces actuales

    //Barra de vida peces
    public Image healthBar; // La referencia a la UI de la barra de vida
    public float healthDecreaseRate = 1f; // Cu�nto disminuye la salud por segundo o por cierto tiempo
    public float healthRecoveryAmount = 20f; // Cu�nta salud se recupera al alimentar a los peces

    private float maxHealth = 100f;
    public float currentHealth;
    private bool fishPurchased = false;

    [SerializeField] public TextMeshProUGUI deathMessageText;



    public int feedCount = 1; // Contador de veces que se ha alimentado a los peces
    private int maxFeedCount = 1; // M�ximo de veces que se puede alimentar antes de recargar
    private int feedPrice = 2; // Costo de recargar la comida
    public Button buyFeedButton; // Bot�n para comprar comida
    public TextMeshProUGUI noFoodText;

    public GameObject recompensaPanel; // Panel que contiene la recompensa
    private bool timerActive = false;
    private float timerStart;

    public Timer timer; // Referencia al componente Timer
    public Button claimRewardButton;

    //public GameObject levelCompletePanel; // Aseg�rate de asignar esto en el inspector

    public Button cleanParticlesButton; // Bot�n para activar la limpieza de part�culas
    private bool particlesCleanPurchased = false; // Estado de la compra de la limpieza de part�culas
    private int cleanParticlesPrice = 10; // Precio de la limpieza de part�culas

    public GameObject decorationPanel;

    void Start()
    {
        feedFishButton.onClick.AddListener(FeedFish);
        buyFeedButton.onClick.AddListener(BuyFeed);
        currentHealth = maxHealth;
        //StartCoroutine(DecreaseHealthOverTime());
        healthBar.gameObject.SetActive(false);

        ValuesController valuesController = FindObjectOfType<ValuesController>();
        valuesController.OnPHToDefault += HandleHealthRecovery;
        valuesController.OnTemperatureToDefault += HandleHealthRecovery;

        noFoodText.gameObject.SetActive(false);

        recompensaPanel.SetActive(false); // Asegura que el panel de recompensas est� oculto al inicio
        claimRewardButton.onClick.AddListener(Recompensa);

        cleanParticlesButton.onClick.AddListener(CleanParticles);
        cleanParticlesButton.interactable = false; // El bot�n inicia desactivado

        UpdateBuyButtonStates();
        UpdateFoodCountDisplay();
    }


    public void BuyParticleCleaning()
    {
        if (moneyManager.CanAfford(cleanParticlesPrice))
        {
            moneyManager.SubtractCoins(cleanParticlesPrice);
            particlesCleanPurchased = true;
            cleanParticlesButton.interactable = true; // Activa el bot�n solo despu�s de la compra
            Debug.Log("Limpieza de part�culas comprada.");
        }
        else
        {
            Debug.Log("No tienes suficientes monedas para comprar la limpieza de part�culas.");
        }
    }

    private void CleanParticles()
    {
        if (particlesCleanPurchased)
        {
            DirtParticleManager.Instance.ClearParticles(); // Aseg�rate de que esta llamada corresponde a la implementaci�n correcta
            particlesCleanPurchased = false;
            cleanParticlesButton.interactable = false; // Desactiva el bot�n despu�s de la limpieza
        }
    }


    public void BuySaltwaterFish(int fishType)
    {
        if (currentFishCount >= maxFishCount)
        {
            Debug.Log("L�mite de peces alcanzado.");
            return;
        }

        int price = saltwaterFishPrices[fishType];
        if (moneyManager.CanAfford(price))
        {
            moneyManager.SubtractCoins(price);
            currentFishCount++;
            fishPurchased = true;

            if (currentFishCount == 1)
            {
                healthBar.gameObject.SetActive(true);
                StartCoroutine(DecreaseHealthOverTime());

                // Iniciar o asegurar que el temporizador est� corriendo
                Timer timerScript = FindObjectOfType<Timer>();
                if (timerScript != null)
                {
                    timerScript.StartTimer(); // Aseg�rate de que este m�todo est� accesible y correctamente definido en Timer
                }
            }

            UpdateHealthBar();
            UpdateBuyButtonStates();
            tareas.CheckFishPurchase(fishType);
        }
    }

    public void Recompensa()
    {
        int dineroRecompensa = 100;
        Debug.Log($"A punto de a�adir {dineroRecompensa} monedas.");
        moneyManager.AddCoins(dineroRecompensa);
        recompensaPanel.SetActive(false);
    }

    private void CheckFishStatusAndReward()
    {
        if (currentHealth > 0 && currentFishCount > 0)
        {
            // Si los peces est�n vivos y saludables, mostrar la recompensa
            recompensaPanel.SetActive(true);
        }
        else
        {
            // Puedes mostrar un mensaje o manejar el caso donde los peces no est�n en buen estado
            Debug.Log("No se puede dar recompensa, los peces est�n en mal estado o no hay peces.");
        }
    }
    public void BuyDecoration(int decorationIndex)
    {
        int price = decorationPrices[decorationIndex];
        if (moneyManager.CanAfford(price))
        {
            moneyManager.SubtractCoins(price);
            // C�digo para a�adir la decoraci�n al juego

            // Iniciar coroutine para mostrar y ocultar el panel
            StartCoroutine(ShowDecorationPanel(3)); // Ajusta el tiempo seg�n necesites

            // Actualizar el progreso de la tarea de decoraci�n
            tareas.CheckDecorationPurchase(decorationIndex);
            UpdateBuyButtonStates();
        }
    }

    private IEnumerator ShowDecorationPanel(float duration)
    {
        decorationPanel.SetActive(true);
        yield return new WaitForSeconds(duration);
        decorationPanel.SetActive(false);
    }


    void OnEnable()
    {
        MoneyManager.OnMoneyChanged += UpdateBuyButtonStates; // Suscribirse al evento
    }

    void OnDisable()
    {
        MoneyManager.OnMoneyChanged -= UpdateBuyButtonStates; // Desuscribirse del evento
    }

    public void UpdateBuyButtonStates()
    {
        bool canBuyMoreFish = currentFishCount < maxFishCount;
        int fishAvailable = 10; // Inicialmente solo 10 peces disponibles

        if (tareas.currentLevel == 2)
            fishAvailable = 20; // En nivel 2, 20 peces disponibles
        else if (tareas.currentLevel >= 3)
            fishAvailable = saltwaterFishPrices.Length; // En nivel 3, todos los peces disponibles

        for (int i = 0; i < buySaltwaterFishButtons.Length; i++)
        {
            buySaltwaterFishButtons[i].interactable = canBuyMoreFish && i < fishAvailable && moneyManager.CanAfford(saltwaterFishPrices[i]);
        }

        for (int i = 0; i < buyDecorationButtons.Length; i++)
        {
            buyDecorationButtons[i].interactable = moneyManager.CanAfford(decorationPrices[i]);
        }

        buyFeedButton.interactable = moneyManager.CanAfford(feedPrice);
        cleanParticlesButton.interactable = particlesCleanPurchased;
        Debug.Log("El bot�n de comprar comida est� " + (buyFeedButton.interactable ? "activado" : "desactivado"));
    }

    public void BuyFeed()
    {
        if (moneyManager.CanAfford(feedPrice))
        {
            moneyManager.SubtractCoins(feedPrice);
            feedCount += maxFeedCount; // Recarga las veces que se puede alimentar
            UpdateFoodCountDisplay();  // Actualiza la UI
            feedFishButton.interactable = true; // Reactiva el bot�n de alimentar
            noFoodText.gameObject.SetActive(false);
            Debug.Log("Comida comprada. Veces restantes para alimentar: " + feedCount);

            // Actualizar la tarea de compra de comida
            tareas.CheckFishFoodPurchase();
        }
        else
        {
            Debug.Log("No hay suficientes monedas para comprar comida.");
        }
    }

    public TextMeshProUGUI foodCountText;
    private void UpdateFoodCountDisplay()
    {
        foodCountText.text = "" + feedCount;
    }
    public void FeedFish()
    {
        // Verifica si hay comidas disponibles antes de alimentar
        if (feedCount > 0)
        {
            feedCount--; // Disminuye el contador de comidas disponibles
            fishFoodObject.SetActive(true);
            GameObject foodInstance = Instantiate(fishFoodObject, FishTankManager.Singleton.GetPointInsideFishTank(), Quaternion.identity);
            Debug.Log("Alimentando peces");
            noFoodText.gameObject.SetActive(false);
            UpdateFoodCountDisplay();  // Actualiza la UI

            StartCoroutine(RemoveFoodAfterSeconds(foodInstance, 20f)); // Inicia la coroutine para eliminar la comida despu�s de cierto tiempo

            if (currentHealth < maxHealth)
            {
                currentHealth += healthRecoveryAmount;
                currentHealth = Mathf.Min(currentHealth, maxHealth); // Asegurarse de no exceder la salud m�xima
                UpdateHealthBar();
            }

            if (feedCount == 0)
            {
                Debug.Log("No hay comida disponible, compra m�s.");
                // Opcional: desactivar el bot�n de alimentar hasta que se compre m�s comida
                noFoodText.gameObject.SetActive(true);
                feedFishButton.interactable = false;
                
            }
        }
        else
        {
            Debug.Log("No hay comida disponible, compra m�s.");
            noFoodText.gameObject.SetActive(true);
            // Opcional: Mostrar alguna alerta en la UI que indique que no hay comida
        }
    }


    private IEnumerator RemoveFoodAfterSeconds(GameObject food, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(food);
    }
    private IEnumerator DecreaseHealthOverTime()
    {
        while (true)
        {
            // Verifica si se han comprado peces y la salud es mayor que 0 para empezar a disminuir
            if (fishPurchased && currentHealth > 0)
            {
                yield return new WaitForSeconds(10); // Espera un segundo antes de disminuir nuevamente la salud
                currentHealth -= healthDecreaseRate;
                currentHealth = Mathf.Max(currentHealth, 0); // Asegura que la salud no sea negativa
                UpdateHealthBar();

                // Aqu� puedes manejar lo que sucede cuando la salud llega a 0, si es necesario
                if (currentHealth <= 0)
                {
                    RemoveRandomFish(); // Opcional: elimina un pez cuando la salud llega a 0
                                        // Considera si necesitas restablecer currentHealth aqu� o en otra parte
                }
            }
            else
            {
                yield return null; // Espera el pr�ximo frame para verificar de nuevo
            }
        }
    }
    void UpdateHealthBar()
    {
        if (!healthBar.gameObject.activeSelf)
        {
            healthBar.gameObject.SetActive(true);
        }
        healthBar.fillAmount = currentHealth / maxHealth;

        // Cambiar el color de la barra de vida dependiendo de la salud actual
        if (currentHealth <= maxHealth * 0.25) // Si la salud es menor o igual al 25% del m�ximo
        {
            healthBar.color = Color.red; // Cambia el color de la barra a rojo
        }
        else if (currentHealth <= maxHealth * 0.5) // Si la salud es menor o igual al 50% del m�ximo
        {
            healthBar.color = Color.yellow; // Cambia el color de la barra a amarillo
        }
        else
        {
            healthBar.color = Color.green; // Si la salud est� por encima del 50%, la barra es verde
        }
    }

    private void RemoveRandomFish()
    {
        if (InstanciarPeces.allFish.Count > 0)
        {
            int indexToRemove = Random.Range(0, InstanciarPeces.allFish.Count);
            GameObject fishToRemove = InstanciarPeces.allFish[indexToRemove];
            InstanciarPeces.allFish.RemoveAt(indexToRemove);
            Destroy(fishToRemove);
            currentFishCount--; // Decrementa el contador de peces

            // Si a�n quedan peces, restablece la salud y actualiza la barra de vida
            if (InstanciarPeces.allFish.Count > 0)
            {
                currentHealth = maxHealth; // Restablece la salud al m�ximo para los peces restantes
                UpdateHealthBar(); // Actualiza la barra de vida para reflejar el cambio
            }
            else
            {
                // Si no quedan peces, desactiva la barra de vida
                healthBar.gameObject.SetActive(false);
            }

            // Actualiza el estado de los botones de compra despu�s de eliminar un pez
            UpdateBuyButtonStates();

            ShowDeathMessage(); // Muestra el mensaje cuando un pez es eliminado
        }
    }

    private void ShowDeathMessage()
    {
        deathMessageText.text = "�Un pez ha muerto de hambre!";
        deathMessageText.gameObject.SetActive(true);
        Invoke("HideDeathMessage", 3); // Espera 5 segundos antes de ocultar el mensaje
    }

    private void HideDeathMessage()
    {
        deathMessageText.gameObject.SetActive(false);
    }


    private void HandleHealthChangeDueToPHOrTemp()
    {
        // Solo ajusta la salud si al menos un pez ha sido comprado
        if (fishPurchased)
        {
            currentHealth *= 0.9f; // Reduce la salud en un 10%
            UpdateHealthBar();

            // Verifica si la salud llega a 0 y maneja la situaci�n adecuadamente
            if (currentHealth <= 0)
            {
                // Aqu� puedes a�adir lo que sucede cuando la salud llega a 0
                RemoveRandomFish(); // Por ejemplo, podr�as eliminar un pez
            }
        }
    }

    private void HandleHealthRecovery()
    {
        if (fishPurchased)
        {
            currentHealth += maxHealth * 0.05f; // Incrementa la salud en un 5%
            currentHealth = Mathf.Min(currentHealth, maxHealth); // Asegura no exceder la salud m�xima
            UpdateHealthBar();
        }
    }

}