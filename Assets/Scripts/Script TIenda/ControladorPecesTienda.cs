using System.Collections.Generic;
using UnityEngine;

public class ControladorPecesTienda : MonoBehaviour
{
    public InstanciarPeces fishManager; // Referencia al script InstanciarPeces
    public GameObject[] saltwaterFishPrefabs; // Array de prefabs de peces de agua salada
    public Tareas tareas;
    //public GameObject[] freshwaterFishPrefabs; // Array de prefabs de peces de agua dulce

    // M�todo que se llamar� cuando se presione el bot�n de comprar pez de agua salada
    public void OnSaltwaterFishButtonClick(int fishType)
    {
        if (fishType < saltwaterFishPrefabs.Length && fishType < (tareas.currentLevel == 1 ? 10 : (tareas.currentLevel == 2 ? 20 : saltwaterFishPrefabs.Length)))
        {
            fishManager.InstantiatedAddFish(new List<GameObject>() { saltwaterFishPrefabs[fishType] });
            Debug.Log("Se llama al script Controlador Peces Tienda para comprar pez de agua salada.");
        }
        else
        {
            Debug.LogError("Intento de comprar un pez no disponible por el nivel actual.");
        }
    }
}