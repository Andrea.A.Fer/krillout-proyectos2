using UnityEngine;

public class LogicaBrillo : MonoBehaviour
{
    public float initialBrightness = 1.0f; // Brillo inicial de la escena (1.0 = normal, 0.0 = totalmente oscuro)
    public float minBrightness = 0.0f; // Brillo m�nimo
    public float maxBrightness = 1.0f; // Brillo m�ximo

    private Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
        float savedBrightness = PlayerPrefs.GetFloat("brillo", initialBrightness);
        SetBrightness(savedBrightness);
    }

    public void ChangeBrightness(float value)
    {
        float brightness = Mathf.Lerp(minBrightness, maxBrightness, value);
        SetBrightness(brightness);
        PlayerPrefs.SetFloat("brillo", brightness);
    }

    private void SetBrightness(float brightness)
    {
        mainCamera.backgroundColor = new Color(brightness, brightness, brightness, 1.0f);
    }
}
