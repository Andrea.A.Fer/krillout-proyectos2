using UnityEngine;
using UnityEngine.UI;

public class LogicaVolumen : MonoBehaviour
{
    public Slider slider;
    public float sliderValue;
    public Image imageMute;
    public Image imagenSound;

    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("VolumenAudio", 0.5f);
        AudioListener.volume = slider.value;
        RevisarSiEstoyMute();
    }

    public void ChangeSlider(float valor)
    {
        sliderValue = valor;
        PlayerPrefs.SetFloat("VolumenAudio", sliderValue);
        AudioListener.volume = sliderValue;
        RevisarSiEstoyMute();
    }

    public void RevisarSiEstoyMute()
    {
        if (sliderValue == 0)
        {
            imageMute.enabled = true;
            imagenSound.enabled = false;
        }
        else
        {
            imageMute.enabled = false;
            imagenSound.enabled = true;
        }
    }

    // Add this method to update the slider value when the options menu is opened
    public void UpdateSliderValue()
    {
        slider.value = PlayerPrefs.GetFloat("VolumenAudio", 0.5f);
        ChangeSlider(slider.value);
    }
}

