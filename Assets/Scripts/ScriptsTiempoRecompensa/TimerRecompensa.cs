using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    public TextMeshProUGUI timerText;
    private float startTime;
    public bool timerActive = false; // Hacerlo p�blico si necesitas acceder desde otro script
    private float checkInterval = 300; // Intervalo de 5 minutos en segundos
    private float nextCheckTime; // Tiempo para el pr�ximo chequeo

    public Tienda tienda;

    void Start()
    {
        // Si quieres que el temporizador empiece inactivo, quita esta llamada desde Start
        // StartTimer();
    }

    public void StartTimer()
    {
        if (!timerActive) // Verifica si el temporizador ya est� activo para evitar reinicios
        {
            startTime = Time.time;
            nextCheckTime = startTime + checkInterval;
            timerActive = true;
        }
    }

    void Update()
    {
        if (timerActive)
        {
            float elapsedTime = Time.time - startTime;
            UpdateTimerDisplay(elapsedTime);

            if (Time.time >= nextCheckTime)
            {
                if (ConditionToCheck())
                {
                    tienda.recompensaPanel.SetActive(true);
                }
                nextCheckTime += checkInterval; // Programa el pr�ximo chequeo
            }
        }
    }

    void UpdateTimerDisplay(float elapsedTime)
    {
        int minutes = (int)elapsedTime / 60;
        int seconds = (int)elapsedTime % 60;
        timerText.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }

    private bool ConditionToCheck()
    {
        // Implementa la l�gica de comprobaci�n aqu�
        return true;
    }
}
