using UnityEngine;
using System.Collections;

public class PanelController : MonoBehaviour
{
    public GameObject panel; // Asigna aqu� tu panel desde el editor de Unity

    // M�todo para activar el panel y empezar la cuenta regresiva para desactivarlo
    public void TogglePanel()
    {
        panel.SetActive(true); // Activa el panel
        StartCoroutine(DeactivateAfterTime(2)); // Espera 15 segundos antes de desactivar el panel
    }

    // Corrutina que espera una cantidad de segundos antes de desactivar el panel
    private IEnumerator DeactivateAfterTime(float time)
    {
        yield return new WaitForSeconds(time); // Espera los segundos definidos
        panel.SetActive(false); // Desactiva el panel
    }
}
